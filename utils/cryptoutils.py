#!/usr/bin/env python2

import string

def generate_dict(choice):
	temporary_dict = {}

	if choice == 'alpha':
		for counter in range(0, len(string.ascii_lowercase)):
			temporary_dict[string.ascii_lowercase[counter]] = counter
	elif choice == 'number':
		for counter in range(0, len(string.ascii_lowercase)):
			temporary_dict[counter] = string.ascii_lowercase[counter]

	return temporary_dict
